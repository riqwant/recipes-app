# Recipes App For Marley Spoon

## Stack
- Ruby
- Sinatra Framework
- Contentful API

## Setup
### Run Server
```
git clone https://riqwant@bitbucket.org/riqwant/recipes-app.git
cd recipes-app
bundle install
rackup -p 5000
```

Visit [http://localhost:5000](http://localhost:5000)


### Run Tests
```
rspec -fd
```
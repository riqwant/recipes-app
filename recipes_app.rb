require 'sinatra/base'
require 'contentful'

# The content type to pass to contentful
RECIPE_CONTENT_TYPE = 'recipe'

class RecipesApp < Sinatra::Base
  configure do
    # The contentful ruby client that is intitialized and available across all routes
    # The keys should ideally be in an env file.
    set :contentful_client, Contentful::Client.new(
      space: 'kk2bw5ojx476',
      access_token: '7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c',
    )
  end

  get '/' do
    redirect '/recipes'
  end

  get '/recipes' do
    begin
      recipes = settings.contentful_client.entries(content_type: RECIPE_CONTENT_TYPE) || []
    rescue Contentful::BadRequest, Contentful::UnparsableResource, Contentful::RateLimitExceeded
      # This is where we catch all the errors that the users can potentially see:
      # - notify bug catcher like honeybadger.
      # - business as usual for the user - empty state
      recipes = []
    end

    erb :recipes, locals: {
      recipes: recipes
    }
  end

  get '/recipes/:id' do
    begin
      recipe = settings.contentful_client.entry(params['id'])
    rescue Contentful::BadRequest, Contentful::UnparsableResource, Contentful::RateLimitExceeded
      # This is where we catch all the errors that the users can potentially see:
      # - notify bug catcher like honeybadger.
      # - business as usual for the user - empty state redirect
      recipe = nil
    end

    # Return the user to homepage if recipe is not found.
    # We should throw a flash message to indicate to the user
    redirect '/recipes' and return if !recipe

    erb :recipe, locals: {
      recipe: recipe
    }
  end
end

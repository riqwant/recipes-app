require 'spec_helper'
require_relative '../recipes_app'

RSpec.describe ::RecipesApp do
  def app
    ::RecipesApp
  end

  let(:recipes) do
    [OpenStruct.new(
      id: 'testId',
      fields: {
        title: 'test title',
        description: 'test description',
        photo: photo,
        chef: chef,
        tags: tags,
      }
    )]
  end

  let(:photo) { OpenStruct.new(url: 'testimage.png') }
  let(:chef) { OpenStruct.new(name: 'test chef name') }
  let(:tags) { [OpenStruct.new(name: 'tag1')] }

  describe 'GET /' do
    it 'redirects to /recipes' do
      get '/'

      expect(last_response.headers['Location']).to include('/recipes')
      expect(last_response.status).to eq 302
    end
  end

  describe 'GET recipes' do
    context 'when there are no recipes' do
      it 'displays a default empty state text' do
        allow(app.settings.contentful_client).to receive(:entries).and_return([])

        get '/recipes'

        expect(last_response.body).to include('Stay tuned, recipes do be cookin\'!')
        expect(last_response.status).to eq 200
      end
    end

    context 'when recipes are available' do
      it 'displays the title and image of the recipe' do
        allow(app.settings.contentful_client).to receive(:entries).and_return(recipes)

        get '/recipes'

        expect(last_response.body).to include('test title')
        expect(last_response.body).to include('testimage.png')
        expect(last_response.status).to eq 200
      end
    end
  end

  describe 'GET recipes/:id' do
    context 'when the ID is invalid' do
      it 'redirects to recipes page' do
        allow(app.settings.contentful_client).to receive(:entries).and_return([])
        allow(app.settings.contentful_client).to receive(:entry).and_return(nil)

        get '/recipes/invalid'

        expect(last_response.headers['Location']).to include('/recipes')
        expect(last_response.status).to eq 302
      end
    end

    context 'when the ID is valid' do
      context 'when errors are not raised' do
        before(:each) do
          allow(app.settings.contentful_client).to receive(:entry).and_return(recipes.first)

          get "/recipes/#{recipes.first.id}"

          expect(last_response.status).to eq 200
        end

        it 'displays the title, description image, chefs name, tags of the recipe' do
          expect(last_response.body).to include('test title')
          expect(last_response.body).to include('testimage.png')
          expect(last_response.body).to include('test description')
          expect(last_response.body).to include('test chef name')
          expect(last_response.body).to include('tag1')
        end

        context 'when chefs name is not present' do
          let(:chef) { nil }

          it 'adds default unassigned text' do
            expect(last_response.body).to include('Chef Unassigned')
          end
        end

        context 'when tags are not present' do
          let(:tags) { nil }

          it 'adds default no tags text' do
            expect(last_response.body).to include('No Tags')
          end
        end
      end

      context 'when errors are raised' do
        before(:each) do
          allow(app.settings.contentful_client).to receive(:entries).and_return([])
          allow(app.settings.contentful_client).to receive(:entry).and_raise(error)

          get "/recipes/#{recipes.first.id}"
        end

        context 'when API triggers an UnparsableResource error' do
          let(:error) do
            Contentful::UnparsableResource.new
          end

          it 'redirects to recipes page' do
            expect(last_response.status).to eq 302
            expect(last_response.headers['Location']).to include('/recipes')
          end
        end

        context 'when API triggers an 400 error' do
          let(:error) do
            Contentful::Error[400].new(
              Contentful::Response.new(OpenStruct.new())
            )
          end

          it 'redirects to recipes page' do
            expect(last_response.status).to eq 302
            expect(last_response.headers['Location']).to include('/recipes')
          end
        end

        context 'when API triggers an rate exceeded error' do
          let(:error) do
            Contentful::Error[429].new(
              Contentful::Response.new(OpenStruct.new())
            )
          end

          it 'redirects to recipes page' do
            expect(last_response.status).to eq 302
            expect(last_response.headers['Location']).to include('/recipes')
          end
        end
      end
    end
  end
end
